//
//  APIManager.swift
//  News Client
//
//  Created by Kalin Spassov on 18/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import Foundation
import Alamofire

final class APIManager {
    private struct Endpoints {
        static let newsApi = "http://newsapi.org/v2/"
        static let headlines = "top-headlines?"
        static let sources = "sources?"
    }
    
    private struct Consts {
        static let newsApiKeyParam = "apiKey="
        static let pageSizeParam = "pageSize="
        static let pageParam = "page="
//        static let languageParam = "language="
        static let sourcesParam = "sources="
        
//        static let languageDefaultValue = "en"
        static let newsApiKeyValue = "eb5b392d0af040b0990286e8010f33ea"
        static let pageSizeDefaultValue = 25
    }
    
    static func getNewsArticles(provider: String, page: Int, success: @escaping ([NewsItemsPayload.NewsItem], Bool)->(), failure: @escaping (String)->()) {
        AF.request(APIManager.generateNewsUrl(provider: provider, page: page))
            .responseJSON { (response) in
                
                switch response.result {
                case .success(_):
                    do {
                        if let data = response.data {
                            let newsItems = try JSONDecoder().decode(NewsItemsPayload.self, from: data)
                            if newsItems.articles.count > 0 {
                                let isLast = (page + 1) * Consts.pageSizeDefaultValue >= newsItems.totalResults
                                success(newsItems.articles, isLast)
                            } else {
                                failure("No items delivered.")
                            }
                        } else {
                            failure("Issue parsing news items.")
                        }
                    } catch {
                        failure("Issue parsing news items.")
                    }
                case .failure(_):
                    failure("Service unavailable.")
                }
        }
    }
    
    static func getNewsProviders(success: @escaping ([ProviderPayload.Source])->(), failure: @escaping (String)->()) {
        AF.request(APIManager.generateProvidersUrl())
            .responseJSON { (response) in
                
                switch response.result {
                case .success(_):
                    do {
                        if let data = response.data {
                            let providers = try JSONDecoder().decode(ProviderPayload.self, from: data)
                            if providers.sources.count > 0 {
                                success(providers.sources)
                            } else {
                                failure("No items delivered.")
                            }
                        } else {
                            failure("Issue parsing providers.")
                        }
                    } catch {
                        failure("Issue parsing providers.")
                    }
                case .failure(_):
                    failure("Service unavailable.")
                }
        }
    }
    
    private static func generateProvidersUrl() -> String {
        return Endpoints.newsApi + Endpoints.sources + "&" + Consts.newsApiKeyParam + Consts.newsApiKeyValue
    }
    
    private static func generateNewsUrl(provider: String ,page: Int = 0, pageSize: Int = Consts.pageSizeDefaultValue) -> String {
        return Endpoints.newsApi + Endpoints.headlines + Consts.newsApiKeyParam + Consts.newsApiKeyValue + "&" + Consts.pageParam + "\(page)" + "&"  + Consts.pageSizeParam + "\(pageSize)" + "&" + Consts.sourcesParam + provider
//        + Consts.languageParam + Consts.languageDefaultValue + "&"
    }
}
