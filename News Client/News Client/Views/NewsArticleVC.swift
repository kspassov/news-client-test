//
//  NewsItem.swift
//  News Client
//
//  Created by Kalin Spassov on 18/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import UIKit
import WebKit

final class NewsArticleVC: UIViewController, StoryboardInstantiatable, WKNavigationDelegate {
    
    @IBOutlet private weak var webView: WKWebView!
    
    var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUrl()
    }
    
    private func loadUrl() {
        guard let url = url else { return }
        let urlRequest = URLRequest(url: url)
        webView.load(urlRequest)
        webView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
}
