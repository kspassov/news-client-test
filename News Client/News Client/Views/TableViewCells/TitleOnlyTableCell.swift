//
//  NewsTableInfoCell.swift
//  News Client
//
//  Created by Kalin Spassov on 18/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import UIKit

final class TitleOnlyTableCell: UITableViewCell {
    struct ViewModel {
        let title: String
    }
    
    @IBOutlet weak var title: UILabel!
    
    var item: ViewModel? {
        didSet {
            title.text = item?.title
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
