//
//  NewsTableCell.swift
//  News Client
//
//  Created by Kalin Spassov on 18/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import UIKit

final class DetailedTableCell: UITableViewCell {
    struct ViewModel {
        let title: String
        let subTitle: String
        let trailingTop: String
        let trailingBot: String
        let largeTextBox: String?
    }
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var trailingTopLabel: UILabel!
    @IBOutlet weak var trailingBotLabel: UILabel!
    @IBOutlet weak var largeText: UILabel!
    
    var item: ViewModel? {
        didSet {
            title.text = item?.title
            subtitle.text = item?.subTitle
            trailingTopLabel.text = item?.trailingTop
            trailingBotLabel.text = item?.trailingBot
            if let largeTextBox = item?.largeTextBox {
                largeText.isHidden = false
                largeText.text = largeTextBox
            } else {
                largeText.isHidden = true
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
