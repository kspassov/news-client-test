//
//  ViewController.swift
//  News Client
//
//  Created by Kalin Spassov on 17/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import UIKit

typealias TableViewManager = UITableViewDelegate & UITableViewDataSource

final class NewsVC: UIViewController, StoryboardInstantiatable {

    @IBOutlet weak private var newsTable: UITableView!
    
    private var articles: [NewsItemsPayload.NewsItem] = []
    private var currentPage = 0
    private var isLast = false
    var provider: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        newsTable.register(cellType: DetailedTableCell.self)
        newsTable.register(cellType: TitleOnlyTableCell.self)
        newsTable.delegate = self
        newsTable.dataSource = self
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshTable), for: UIControl.Event.valueChanged)
        newsTable.refreshControl = refreshControl
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTable), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshTable()
    }

    func getNewsItemsSuccess(items: [NewsItemsPayload.NewsItem], isLast: Bool) {
        let previousCounter = articles.count
        currentPage += 1
        articles.append(contentsOf: items)
        self.isLast = isLast
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            // reload the last cell, so it now longer shows loading
            self.newsTable.beginUpdates()
            self.newsTable.reloadRows(at: [IndexPath(row: previousCounter, section: 0)], with: .automatic)
            let indexPaths = ((previousCounter + 1)...self.articles.count).map({ return IndexPath(row: $0, section: 0) })
            self.newsTable.insertRows(at: indexPaths, with: .automatic)
            self.newsTable.endUpdates()
        }
    }
    
    func getNewsItemsFailure(error: String) {
        isLast = true
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.newsTable.reloadData()
        }
        
    }
    
    private func getMoreNewsItems() {
        if let provider = self.provider, !isLast {
            APIManager.getNewsArticles(provider: provider, page: currentPage, success: getNewsItemsSuccess, failure: getNewsItemsFailure)
        }
    }
    
    private func resetInternalParams() {
        isLast = false
        currentPage = 0
    }
    
    @objc func refreshTable() {
        resetInternalParams()
        articles.removeAll()
        newsTable.reloadData()
        newsTable.refreshControl?.endRefreshing()
        getMoreNewsItems()
        
    }
}

// MARK: UITableview Delegate & Datasource
extension NewsVC: TableViewManager {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if articles.count > 0 {
            if indexPath.row == articles.count { // last cell in table, need to load more.
                getMoreNewsItems()
                
                let cell = tableView.dequeueReusableCell(with: TitleOnlyTableCell.self, for: indexPath)
                let title = isLast ? "No more articles available." : "Loading even more articles for you..."
                cell.item = TitleOnlyTableCell.ViewModel(title: title)
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(with: DetailedTableCell.self, for: indexPath)
                let item = articles[indexPath.row]
                cell.item = DetailedTableCell.ViewModel(title: item.title ?? "", subTitle: item.author ?? "", trailingTop: item.source?.name ?? "", trailingBot: Date.fromString(item.publishedAt ?? "")?.toString() ?? "", largeTextBox: item.description)
            
            return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(with: TitleOnlyTableCell.self, for: indexPath)
            let title = isLast ? "No articles from the selected news source." : "Loading articles..." // I know I'm misusing the isLast for an error, but time constraints.
            cell.item = TitleOnlyTableCell.ViewModel(title: title)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard articles.count > indexPath.row, let url = URL(string: articles[indexPath.row].url ?? "") else { return }
        let descVC = NewsArticleVC.instantiate()
        descVC.url = url
        self.navigationController?.pushViewController(descVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}
