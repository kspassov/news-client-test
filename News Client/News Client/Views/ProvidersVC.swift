//
//  ProvidersVC.swift
//  News Client
//
//  Created by Kalin Spassov on 18/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import UIKit

final class ProvidersVC: UIViewController, StoryboardInstantiatable {
    
    @IBOutlet weak var providersTable: UITableView!
    
    private var providers: [ProviderPayload.Source] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        providersTable.register(cellType: DetailedTableCell.self)
        providersTable.register(cellType: TitleOnlyTableCell.self)
        providersTable.delegate = self
        providersTable.dataSource = self
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshNewsProviders), for: UIControl.Event.valueChanged)
        providersTable.refreshControl = refreshControl
        getNewsProviders()
    }
    
    @objc private func refreshNewsProviders() {
        providersTable.refreshControl?.endRefreshing()
        getNewsProviders()
    }
    
    private func getNewsProviders() {
        APIManager.getNewsProviders(success: getProvidersSuccess, failure: getProviderFailure)
    }
    
    func getProvidersSuccess(items: [ProviderPayload.Source]) {
        if items.count > 0 {
            providers = items
            providersTable.reloadData() // No need to reload cells, it replaces the whole chunk, there is no paging option in this api call.
        }
    }
    
    func getProviderFailure(error: String) {
        
    }
    
}

extension ProvidersVC: TableViewManager {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if providers.count > 0 {
            return providers.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if providers.count > 0 {
            let cell = tableView.dequeueReusableCell(with: DetailedTableCell.self, for: indexPath)
            let item = providers[indexPath.row]
            cell.item = DetailedTableCell.ViewModel(title: item.name ?? "", subTitle: item.description ?? "", trailingTop: item.category ?? "", trailingBot: item.language ?? "", largeTextBox: nil)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(with: TitleOnlyTableCell.self, for: indexPath)

            cell.item = TitleOnlyTableCell.ViewModel(title: "Loading news sources...") // Might be overkill but I like it.

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let providerId = providers[indexPath.row].id {
            let descVC = NewsVC.instantiate()
            descVC.provider = providerId
            self.navigationController?.pushViewController(descVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
}
