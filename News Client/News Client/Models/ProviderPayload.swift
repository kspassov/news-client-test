//
//  Provider.swift
//  News Client
//
//  Created by Kalin Spassov on 18/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import Foundation

struct ProviderPayload: Codable {
    struct Source: Codable {
        let id: String?
        let name: String?
        let description: String?
        let url: String?
        let category: String?
        let language: String?
        let country: String?
    }
    
    let sources: [Source]
    let status: String
}
