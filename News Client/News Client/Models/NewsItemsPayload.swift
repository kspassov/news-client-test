//
//  NewsItem.swift
//  News Client
//
//  Created by Kalin Spassov on 18/04/2020.
//  Copyright © 2020 KaliSpass. All rights reserved.
//

import Foundation

struct NewsItemsPayload: Codable {
    struct NewsItem: Codable {
        struct Source: Codable {
            let id: String?
            let name: String?
        }
        
        
        
        let source: Source?
        let author: String?
        let title: String?
        let description: String? // lazy var url: URL?
        let url: String?
        let urlToImage: String?
        let publishedAt: String?
        let content: String?
    }
    
    let status: String
    let totalResults: Int
    let articles: [NewsItem]
}
