//
//  DateExtensions.swift
//  KaliTest
//
//  Created by Kali Spassov on 12/05/2019.
//  Copyright © 2019 Kali Spassov. All rights reserved.
//

import Foundation

extension Date {
    static var dateFormatter: DateFormatter = {
        let internalDateFormatter = DateFormatter()
        internalDateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
        return internalDateFormatter
    }()
    
    
    func toString()-> String? {
    
    let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy HH:mm"
    
        return dateFormatter.string(from: self)
    }
    
    static func fromString(_ string: String) -> Date? {
        return Date.dateFormatter.date(from: string)
    }
}
