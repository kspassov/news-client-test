//
//  NotificationName.swift
//  KaliTest
//
//  Created by Kalin Spassov on 15/05/2019.
//  Copyright © 2019 zemingo. All rights reserved.
//

import Foundation

protocol NotificationName {
    var name: Notification.Name { get }
}

extension RawRepresentable where RawValue == String, Self: NotificationName {
    var name: Notification.Name {
        get {
            return Notification.Name(self.rawValue)
        }
    }
}
