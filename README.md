# News Client #

This app is meant to show global news with paging and cell control.

### Features ###

* App loads items in chunks of 25 to reduce memory use
* Pull-to-refresh
* Select cell to open in Safari

### Design Principles ###

* Basic MVVM Architecture for cells
* Basic MVC (there wasn't much need for MVC here)
* Used AlamoFire for comminucations

### Future TODO List ###
I started working on the NewsVC and only after finishing it realised I can merge it with the ProvidersVC and just swap a datasource, but that was too later into the test and I wanted to try and stay in the 4 hour scope. Given enough time they would have a data provider gived to them which will give them the items they need via delegation.

Design is sinfully ugly, I focused on features like pull to refresh and the paging (It can send up to 700 items, I didn't want to reloadData on that each time). Given time, it would look good with images in the preview and dynamic sized. I don't have much experience with dynamic cell sizes so I wanted to use something I'm familiar with.

There are no unit tests. As mentioned in the interview, that is not something I have experience with and so I didn't implement it.
